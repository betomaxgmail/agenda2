package com.example.agenda2.Modals;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;

import java.util.Calendar;

public class TimePickerFragment extends DialogFragment {
    private int hora;
    private  int minuto;
    public TimePickerFragment(int hora, int minuto){
        this.hora = hora;
        this.minuto = minuto;
    }

    @Override
    public Dialog onCreateDialog(Bundle SaveInstanceState){
        /*Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        int second = c.get(Calendar.SECOND);*/
        return new TimePickerDialog(getActivity(),(TimePickerDialog.OnTimeSetListener) getActivity(), this.hora, this.minuto, true);
    }
}
