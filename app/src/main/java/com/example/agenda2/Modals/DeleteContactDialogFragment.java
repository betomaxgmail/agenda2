package com.example.agenda2.Modals;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialog;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.agenda2.R;

import java.io.Serializable;


public class DeleteContactDialogFragment extends AppCompatDialogFragment {
    //private Context mContext;
    private OnYesNoClick yesNoClick;

    public interface OnYesNoClick extends Parcelable {
        void onYesClicked();
        void onNoClicked();
    }

    public void setOnYesNoClick(OnYesNoClick yesNoClick) {
        this.yesNoClick = yesNoClick;
    }

    public static DeleteContactDialogFragment newInstance(String title, String message, OnYesNoClick event) {
        // Required empty public constructor
        DeleteContactDialogFragment f = new DeleteContactDialogFragment();
        Bundle b = new Bundle();
        b.putString("title", title);
        b.putString("message", message);
        b.putParcelable("event", event);
        // Add bundle data in arguments
        f.setArguments(b);
        return f;
    }

    /*@Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d("OnAttach", "Hola");
        Log.d("OnAttach (Context)", context.toString());
        if (context instanceof OnYesNoClick) {
            yesNoClick = ((OnYesNoClick) context);
        }
    }*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_delete_contact_dialog, container, false);
        TextView txtMessage = (TextView) view.findViewById(R.id.txtMessage);
        Button btnYes = (Button) view.findViewById(R.id.btnYes);
        Button btnNo = (Button) view.findViewById(R.id.btnNo);

        Bundle b = getArguments();
        String message = b.getString("message");
        yesNoClick = b.getParcelable("event");

        DeleteContactDialogFragment me = this;
        txtMessage.setText(message);
        btnNo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(yesNoClick != null) {
                    yesNoClick.onNoClicked();
                    me.dismiss();
                }
                //Toast.makeText(getContext(),"NO", Toast.LENGTH_SHORT).show();
            }
        });

        btnYes.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(yesNoClick != null) {
                    yesNoClick.onYesClicked();
                    me.dismiss();
                }
                //Toast.makeText(getContext(),"OK", Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }

    /*@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null) {
            String title = getArguments().getString("title", "Confirmación");
            String message = getArguments().getString("message", "¿Esta seguro de realizar la operación?");
            getDialog().setTitle(title);
        }
    }*/

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle b = getArguments();
        String title = b.getString("title");
        AppCompatDialog dialogCompat = new AppCompatDialog(getActivity());
        dialogCompat.setTitle(title);
        dialogCompat.setContentView(R.layout.fragment_delete_contact_dialog);
        dialogCompat.getWindow().setLayout(1000, 800);

        // Use the Builder class for convenient dialog construction
        /*AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        Bundle b = getArguments();
        String title = b.getString("title");
        String message = b.getString("message");
        builder.setTitle(title).setMessage(message)
                .setPositiveButton("Sí", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(yesNoClick != null) {
                            yesNoClick.onYesClicked();
                        }
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //if (dialog != null && ((Dialog) dialog).isShowing()) {
                        //    dialog.dismiss();
                        //}
                        if(yesNoClick != null) {
                            yesNoClick.onNoClicked();
                        }
                    }
                });*/
        /*Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));*/
        // Create the AlertDialog object and return it
        return dialogCompat;
    }
}