package com.example.agenda2;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.agenda2.Models.Event.Event;
import com.example.agenda2.Models.Event.EventAdapter;
import com.example.agenda2.Models.Event.EventModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

public class EventListFragment extends Fragment {

    public static final String ARG_OBJECT = "object";
    ArrayList<Event> data;
    ListView listView;
    FloatingActionButton btnAdd;

    public EventListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_event_list, container, false);
    }

    public void showData() {
        this.data = EventModel.getAll(getContext());
        //Toast.makeText(getContext(), String.valueOf(this.data.size()), Toast.LENGTH_LONG).show();
        EventAdapter adapter = new EventAdapter(getActivity(), R.layout.item_event_list, this.data, this);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), EventActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("EVENT_DATA", data.get(position));
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        this.showData();
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Bundle args2 = getArguments();
        //args.getInt(ARG_OBJECT)
        listView = (ListView) view.findViewById(R.id.eventList);
        this.btnAdd = (FloatingActionButton) view.findViewById(R.id.floatingActionButtonN);

        btnAdd.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getActivity(), EventActivity.class);
                        startActivity(intent);
                    }
                }
        );


        this.showData();


    }
}