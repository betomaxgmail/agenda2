package com.example.agenda2;


import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.database.sqlite.SQLiteDatabase;
import android.icu.text.DateFormat;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import com.example.agenda2.Modals.DatePickerFragment;
import com.example.agenda2.Modals.TimePickerFragment;
import com.example.agenda2.Models.Contact.Contact;
import com.example.agenda2.Models.Contact.ContactModel;
import com.example.agenda2.Models.Event.Event;
import com.example.agenda2.Models.Event.EventModel;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class EventActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener , TimePickerDialog.OnTimeSetListener {

    Event event;
    Button btnCancel;
    Button btnSave;
    Button btnFecha;
    EditText txtName;
    //EditText txtContacto;
    EditText txtUbicationName;
    EditText txtUbicacion;
    EditText txtLugar;
    TextView lbFecha;
    Spinner dropdown1;
    Spinner dropdown2;
    TextView lbHora;
    Button btnHora;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        this.dropdown1 = (Spinner) findViewById(R.id.ddcontacto);
        this.dropdown2 = (Spinner) findViewById(R.id.ddTipoEvent);

        this.txtName = (EditText) findViewById(R.id.txtUbicationName);
        this.txtLugar = (EditText) findViewById(R.id.txtLugar);

        Date currentTime = Calendar.getInstance().getTime();
        String hDate = new SimpleDateFormat("HH:mm:ss").format(currentTime);
        this.lbHora = (TextView)findViewById(R.id.lbHora);
        this.lbHora.setText(hDate);

        this.btnHora = (Button) findViewById(R.id.btnHora);
        this.btnHora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(v.getContext(), "Hola btnHora", Toast.LENGTH_SHORT).show();
                try {
                    java.text.DateFormat format = new java.text.SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                    Date date = format.parse(lbFecha.getText().toString() + " " + lbHora.getText().toString());
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(date);
                    DialogFragment timePicker = new TimePickerFragment(cal.get(Calendar.HOUR_OF_DAY),cal.get(Calendar.MINUTE));
                    timePicker.show(getSupportFragmentManager(),"TimePicker");
                }
                catch(Exception e) {
                    Toast.makeText(v.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        //Date currentTime = Calendar.getInstance().getTime();
        String fDate = new SimpleDateFormat("yyyy/MM/dd").format(currentTime);
        this.lbFecha = (TextView)findViewById(R.id.lbFecha);
        this.lbFecha.setText(fDate);

        this.btnFecha = (Button) findViewById(R.id.btnFecha);
        this.btnFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    java.text.DateFormat format = new java.text.SimpleDateFormat("yyyy/MM/dd");
                    Date date = format.parse(lbFecha.getText().toString());
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(date);
                    DialogFragment datePicker = new DatePickerFragment(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
                    datePicker.show(getSupportFragmentManager(), "date picker");
                }
                catch(Exception e) {
                    Toast.makeText(v.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        this.btnSave = (Button) findViewById(R.id.btnSave);
        this.btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(event == null){
                    Contact c = (Contact)dropdown1.getItemAtPosition(dropdown1.getSelectedItemPosition());
                    Toast.makeText(v.getContext(),String.valueOf(c.getId()), Toast.LENGTH_LONG).show();
                    DateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

                    try {
                        Date date = format.parse(lbFecha.getText().toString() + " " + lbHora.getText().toString() );
                        event = new Event(txtName.getText().toString(), c.getId(), dropdown2.getSelectedItem().toString(), date,txtLugar.getText().toString());
                        EventModel.insert(v.getContext(), event);
                        finish();
                    }
                    catch (Exception e) {
                        Toast.makeText(v.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    try {
                        event.setNombre(txtName.getText().toString());
                        event.setLugar(txtLugar.getText().toString());

                        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        Date date2 = format.parse(lbFecha.getText().toString() + " " + lbHora.getText().toString() );
                        event.setFecha(date2);
                        Contact contact = (Contact)dropdown1.getItemAtPosition(dropdown1.getSelectedItemPosition());
                        event.setContacto(contact.getId());
                        event.setTipoEvento(dropdown2.getSelectedItem().toString());


                        EventModel.update(v.getContext(), event);
                        finish();
                    }
                    catch (Exception e) {
                        Toast.makeText(v.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

        this.btnCancel = (Button) findViewById(R.id.btnCancel);
        this.btnCancel.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(v.getContext(), "Hola btn cancelar", Toast.LENGTH_LONG).show();
                finish();
            }
        }));


        ArrayList<Contact> contactos = ContactModel.getAll(this);



        ArrayAdapter<Contact> adapter = new ArrayAdapter<Contact>(this, android.R.layout.simple_spinner_dropdown_item, contactos);
        dropdown1.setAdapter(adapter);
        dropdown1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Contact c = (Contact) parent.getItemAtPosition(position);
                Toast.makeText(view.getContext(),String.valueOf(c.getId())+" "+ c.getName(),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        String[] tipo = new String[]{"Recordatorio", "Evento", "Meta"};
        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, tipo);
        dropdown2.setAdapter(adapter2);

        if(getIntent().getExtras() != null) {
            this.event = (Event) getIntent().getSerializableExtra("EVENT_DATA");

            this.txtName.setText(this.event.getNombre());
            //Toast.makeText(this, event.getNombre(), Toast.LENGTH_LONG).show();
            this.txtLugar.setText(this.event.getLugar());
            int i = 0;
            for (i = 0; i < contactos.size(); i++){
                if (contactos.get(i).getId() == this.event.getContacto() ){
                    break;
                }
            }
            this.dropdown1.setSelection(i);

            i = 0;
            for (i = 0; i < tipo.length; i++){

                if (tipo[i].equals(this.event.getTipoEvento())){
                    //Toast.makeText(this, tipo[i], Toast.LENGTH_LONG).show();
                    break;
                }

            }
            this.dropdown2.setSelection(i);

            String feecha = new SimpleDateFormat("yyyy-MM-dd").format(this.event.getFecha());
            String hora = new SimpleDateFormat("HH:mm:ss").format(this.event.getFecha());
            this.lbHora.setText(hora);
            this.lbFecha.setText(feecha);


        }

        /*
        this.btnSave = (Button) findViewById(R.id.btnYes);
        this.btnCancel = (Button) findViewById(R.id.btnNo);
        this.btnFecha = (Button) findViewById(R.id.btnFecha);
        this.txtUbicationName = (EditText) findViewById(R.id.txtUbicationName);
        this.txtUbicacion = (EditText) findViewById(R.id.txtUbicacion);
        this.txtLugar = (EditText) findViewById(R.id.txtLugar);
        this.lbFecha = (TextView) findViewById(R.id.lbFecha);
        this.dropdown1 = (Spinner) findViewById(R.id.ddcontacto);
        this.dropdown2 = (Spinner) findViewById(R.id.ddTipoEvent);


        String[] contact = new String[]{"Luisa", "Roberto", "Daniel"};
        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, contact);
        dropdown1.setAdapter(adapter1);

        String[] tipo = new String[]{"Recordatorio", "Evento", "Meta"};
        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, tipo);
        dropdown2.setAdapter(adapter2);

        this.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



        this.btnFecha.setOnClickListener(new View.OnClickListener(){

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                try {
                    java.text.DateFormat format = new java.text.SimpleDateFormat("dd/MM/yyyy");
                    Date date = format.parse(lbFecha.getText().toString());
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(date);
                    DialogFragment datePicker = new DatePickerFragment(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
                    datePicker.show(getSupportFragmentManager(), "date picker");
                }
                catch(Exception e) {
                    Toast.makeText(v.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });


        this.btnSave.setOnClickListener(new View.OnClickListener() {

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                if (event == null) {

                    DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                    try {
                        Date date = format.parse(lbFecha.getText().toString());
                        event = new Event(txtUbicationName.getText().toString(), txtUbicacion.getText().toString(),  date, txtLugar.getText().toString());
                        EventModel.insert(v.getContext(), event);
                        finish();
                    }
                    catch (Exception e) {
                        Toast.makeText(v.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                else if (event != null) {
                    try {
                        event.setNombre(txtUbicationName.getText().toString());
                        event.setUbicacion(txtUbicacion.getText().toString());
                        event.setLugar(txtLugar.getText().toString());

                        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                        Date date2 = format.parse(lbFecha.getText().toString());
                        event.setFecha(date2);


                        EventModel.update(v.getContext(), event);
                        finish();
                    }
                    catch (Exception e) {
                        Toast.makeText(v.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        if(getIntent().getExtras() != null) {
            this.event = (Event) getIntent().getSerializableExtra("EVENT_DATA");

            this.txtUbicationName.setText(this.event.getNombre());
            this.txtUbicacion.setText(this.event.getUbicacion());
            this.txtLugar.setText(this.event.getLugar());

            String feecha = new SimpleDateFormat("dd/MM/yyyy").format(this.event.getFecha());
            this . lbFecha.setText(feecha);

        }
        else {

            Date currentTime = Calendar.getInstance().getTime();
            String feecha = new SimpleDateFormat("dd/MM/yyyy").format(currentTime);
            this.lbFecha.setText(feecha);
        }*/
    }



    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        Date currentTime = Calendar.getInstance().getTime();
        String feecha = new SimpleDateFormat("yyyy/MM/dd").format(c.getTime());
        this.lbFecha.setText(feecha);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        this.lbHora.setText(hourOfDay + ":" + minute + ":00");
    }
}