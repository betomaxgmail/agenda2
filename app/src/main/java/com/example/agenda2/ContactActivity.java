package com.example.agenda2;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.icu.text.DateFormat;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.agenda2.Modals.DatePickerFragment;
import com.example.agenda2.Models.Contact.Contact;
import com.example.agenda2.Models.Contact.ContactModel;
import com.example.agenda2.Models.Save;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

public class ContactActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    Contact contact;
    Button btnCancel;
    Button btnSave;
    Button btnDate;
    EditText txtName;
    EditText txtTel;
    EditText txtEmail;
    EditText txtDireccion;
    TextView lbDate;
    CircleImageView image;

    Uri imageUri;
    private static final int PICK_IMAGE = 100;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        if (savedInstanceState != null) {
            imageUri = savedInstanceState.getParcelable("imageUri");
        }

        this.btnSave = (Button) findViewById(R.id.btnYes);
        this.btnCancel = (Button) findViewById(R.id.btnNo);
        this.btnDate = (Button) findViewById(R.id.btnDate);
        this.txtName = (EditText) findViewById(R.id.txtName);
        this.txtTel = (EditText) findViewById(R.id.txtTel);
        this.txtEmail = (EditText) findViewById(R.id.txtEmail);
        this.txtDireccion = (EditText) findViewById(R.id.txtDir);
        this.lbDate = (TextView) findViewById(R.id.lbDate);
        this.image = (CircleImageView) findViewById(R.id.profile_image);

        this.image.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Toast.makeText(v.getContext(), "Hola beto", Toast.LENGTH_SHORT).show();
                openGallery();
            }
        });

        this.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        this.btnDate.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                try {
                    java.text.DateFormat format = new java.text.SimpleDateFormat("dd/MM/yyyy");
                    Date date = format.parse(lbDate.getText().toString());
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(date);
                    DialogFragment datePicker = new DatePickerFragment(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
                    datePicker.show(getSupportFragmentManager(), "date picker");
                }
                catch(Exception e) {
                    Toast.makeText(v.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        this.btnSave.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    if (contact == null) {
                        String uri = imageUri != null ? imageUri.getPath() : "";
                        if(imageUri != null) {
                            BitmapDrawable drawable = (BitmapDrawable) image.getDrawable();
                            Bitmap bmap = drawable.getBitmap();
                            Save savefile = new Save();
                            uri = savefile.SaveImage(v.getContext(), bmap);
                        }

                        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                        try {
                            Date date = format.parse(lbDate.getText().toString());
                            contact = new Contact(txtName.getText().toString(), txtTel.getText().toString(), uri, txtEmail.getText().toString(), txtDireccion.getText().toString(), date);
                            ContactModel.insert(v.getContext(), contact);
                            finish();
                        }
                        catch (Exception e) {
                            Toast.makeText(v.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    else if (contact != null) {
                        try {
                            String uri = imageUri != null ? imageUri.getPath() : "";
                            contact.setName(txtName.getText().toString());
                            contact.setNumber(txtTel.getText().toString());
                            contact.setEmail(txtEmail.getText().toString());
                            contact.setAddress(txtDireccion.getText().toString());
                            if(uri != contact.getImage()) {
                                Save savefile = new Save();
                                savefile.DeleteImage(v.getContext(), contact.getImage());
                                BitmapDrawable drawable = (BitmapDrawable) image.getDrawable();
                                Bitmap bmap = drawable.getBitmap();
                                uri = savefile.SaveImage(v.getContext(), bmap);
                                contact.setImage(uri);
                            }

                            DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                            Date date = format.parse(lbDate.getText().toString());
                            contact.setBirthDate(date);

                            ContactModel.update(v.getContext(), contact);
                            finish();
                        }
                        catch (Exception e) {
                            Toast.makeText(v.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                catch(IOException e) {
                    Toast.makeText(v.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        if(getIntent().getExtras() != null) {
            this.contact = (Contact) getIntent().getSerializableExtra("CONTACT_DATA");

            this.txtName.setText(this.contact.getName());
            this.txtTel.setText(this.contact.getNumber());
            this.txtEmail.setText(this.contact.getEmail());
            this.txtDireccion.setText(this.contact.getAddress());
            String fDate = new SimpleDateFormat("dd/MM/yyyy").format(this.contact.getBirthDate());
            this.lbDate.setText(fDate);

            if(this.imageUri != null) {
                this.image.setImageURI(imageUri);
            }
            else if(this.contact.getImage() != "") {
                this.imageUri = Uri.fromFile(new File(this.contact.getImage()));
                image.setImageURI(this.imageUri);
            }
        }
        else {
            if(this.imageUri != null) {
                this.image.setImageURI(imageUri);
            }

            Date currentTime = Calendar.getInstance().getTime();
            String fDate = new SimpleDateFormat("dd/MM/yyyy").format(currentTime);
            this.lbDate.setText(fDate);
        }
    }

    private void openGallery() {
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, PICK_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == PICK_IMAGE){
            imageUri = data.getData();
            this.image.setImageURI(imageUri);
        }
    }

    public String getRealPath(Uri uri) {
        String yourRealPath = "";
        //Uri uri = data.getData();
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
        if(cursor.moveToFirst()){
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            yourRealPath = cursor.getString(columnIndex);
        } else {
            //boooo, cursor doesn't have rows ...
        }
        cursor.close();

        return yourRealPath;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("imageUri", imageUri);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        Date currentTime = Calendar.getInstance().getTime();
        String fDate = new SimpleDateFormat("dd/MM/yyyy").format(c.getTime());
        this.lbDate.setText(fDate);
    }
}