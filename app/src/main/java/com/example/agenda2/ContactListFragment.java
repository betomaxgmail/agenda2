package com.example.agenda2;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.agenda2.Models.Contact.Contact;
import com.example.agenda2.Models.Contact.ContactAdapter;
import com.example.agenda2.Models.Contact.ContactModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

public class ContactListFragment extends Fragment {
    public String[] names = {"Beto", "Luis"};
    public static final String ARG_OBJECT = "object";
    ArrayList<Contact> data;
    ListView listView;
    FloatingActionButton btnAdd;

    public ContactListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contact_list, container, false);
    }

    public void showData() {
        this.data = ContactModel.getAll(getContext());
        ContactAdapter adapter = new ContactAdapter(getActivity(), R.layout.item_contact_list, this.data, this);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), ContactActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("CONTACT_DATA", data.get(position));
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        this.showData();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Bundle args = getArguments();
        //args.getInt(ARG_OBJECT)
        listView = ((ListView) view.findViewById(R.id.userList));
        btnAdd = (FloatingActionButton) view.findViewById(R.id.floatingActionButton);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getActivity(),"Hola beto",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), ContactActivity.class);
                startActivity(intent);
            }
        });

        this.showData();
    }
}