package com.example.agenda2.Models.Contact;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.agenda2.Models.DbHelper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ContactModel extends DbHelper {

    public ContactModel(Context context) {
        super(context);
    }

    public static long insert(Context context, Contact contact)
    {
        long id = 0;
        SQLiteDatabase db = null;
        try {
            DbHelper dbHelper = new DbHelper(context);
            db = dbHelper.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put("nombre", contact.getName());
            values.put("telefono", contact.getNumber());
            if (contact.getImage() != null || contact.getImage() != "")
                values.put("foto", contact.getImage());
            if (contact.getEmail() != null || contact.getEmail() != "")
                values.put("email", contact.getEmail());
            if (contact.getAddress() != null || contact.getAddress() != "")
                values.put("address", contact.getAddress());
            if (contact.getBirthDate() != null) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                values.put("birthDate", sdf.format(contact.getBirthDate()));
            }


            id = db.insert(TABLE_CONTACTOS, null, values);
            contact.setId((int)id);
        } catch (Exception ex) {
            ex.toString();
        } finally {
            if (db != null && db.isOpen())
                db.close();
        }

        return id;
    }

    public static ArrayList<Contact> getAll(Context context) {

        DbHelper dbHelper = new DbHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ArrayList<Contact> listaContactos = new ArrayList<Contact>();
        Contact contact;
        Cursor cursorContact;

        cursorContact = db.rawQuery("SELECT * FROM " + TABLE_CONTACTOS, null);

        if (cursorContact.moveToFirst()) {
            do {
                String dateString = cursorContact.getString(6);
                DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                try {
                    Date date = format.parse(dateString);
                    contact = new Contact(
                            cursorContact.getInt(0),
                            cursorContact.getString(1),
                            cursorContact.getString(2),
                            cursorContact.getString(3),
                            cursorContact.getString(4),
                            cursorContact.getString(5),
                            date
                    );
                    Log.d("GetAll", contact.getId() + " - " + contact.getName() + " - " + contact.getNumber() + " - " + contact.getImage());
                    listaContactos.add(contact);
                }
                catch (Exception ex) {
                    Log.d("GetAll", ex.getMessage());
                }
            } while (cursorContact.moveToNext());
        }

        cursorContact.close();
        db.close();

        return listaContactos;
    }

    public static Contact getById(Context context, int id) {

        DbHelper dbHelper = new DbHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        Contact contact = null;
        Cursor cursorContact;

        cursorContact = db.rawQuery("SELECT * FROM " + TABLE_CONTACTOS + " WHERE id = " + id + " LIMIT 1", null);

        if (cursorContact.moveToFirst()) {
            String dateString = cursorContact.getString(6);
            DateFormat format = new SimpleDateFormat("dd/MM/yyyy");

            try {
                Date date = format.parse(dateString);
                contact = new Contact(
                        cursorContact.getInt(0),
                        cursorContact.getString(1),
                        cursorContact.getString(2),
                        cursorContact.getString(3),
                        cursorContact.getString(4),
                        cursorContact.getString(5),
                        date
                );
                Log.d("getById", contact.getId() + " - " + contact.getName() + " - " + contact.getNumber() + " - " + contact.getImage());
            }
            catch (Exception ex) {
                Log.d("getById", ex.getMessage());
            }
        }

        cursorContact.close();
        db.close();

        return contact;
    }

    public static boolean update(Context context, Contact contact) {

        boolean status = false;

        DbHelper dbHelper = new DbHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            db.execSQL("UPDATE " + TABLE_CONTACTOS +
                    " SET nombre = '" + contact.getName() +
                    "', telefono = '" + contact.getNumber() +
                    "', foto = '" + contact.getImage() +
                    "', email = '" + contact.getEmail() +
                    "', address = '" + contact.getAddress() +
                    "', birthDate = '" + sdf.format(contact.getBirthDate()) +
                    "' WHERE id='" + contact.getId() + "' ");
            status = true;
        } catch (Exception ex) {
            ex.toString();
            status = false;
        } finally {
            if (db != null && db.isOpen())
                db.close();
        }

        return status;
    }

    public static boolean delete(Context context, Contact contact) {

        boolean status = false;

        DbHelper dbHelper = new DbHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        try {
            db.execSQL("DELETE FROM " + TABLE_CONTACTOS + " WHERE id = '" + contact.getId() + "'");
            status = true;
        } catch (Exception ex) {
            ex.toString();
            status = false;
        } finally {
            if (db != null && db.isOpen())
                db.close();
        }

        return status;
    }
}
