package com.example.agenda2.Models.Contact;

import android.content.Context;
import android.net.Uri;
import android.os.Parcel;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.example.agenda2.ContactListFragment;
import com.example.agenda2.MainActivity;
import com.example.agenda2.Modals.DeleteContactDialogFragment;
import com.example.agenda2.Models.Save;
import com.example.agenda2.R;

import java.io.File;
import java.util.ArrayList;

public class ContactAdapter extends ArrayAdapter<Contact> {
    private ArrayList<Contact> contacts;
    private androidx.fragment.app.Fragment fragment;

    public ContactAdapter(@NonNull Context context, int resource, ArrayList<Contact> contacts, Fragment frag) {
        super(context, resource, contacts);
        this.contacts = contacts;
        this.fragment = frag;
        Log.d("","Constructor ContactAdapter");
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_contact_list, parent, false);
        }

        ImageView image = convertView.findViewById(R.id.profile_image);
        TextView nombre = convertView.findViewById(R.id.nombre);
        TextView num = convertView.findViewById(R.id.numero);
        Button btnDelete = convertView.findViewById(R.id.button);

        btnDelete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Contact contact = contacts.get(position);

                DeleteContactDialogFragment.OnYesNoClick event = new DeleteContactDialogFragment.OnYesNoClick() {
                    @Override
                    public int describeContents() {
                        return 0;
                    }

                    @Override
                    public void writeToParcel(Parcel dest, int flags) {

                    }

                    @Override
                    public void onYesClicked() {
                        try {
                            ContactModel.delete(v.getContext(), contact);
                            Save savefile = new Save();
                            savefile.DeleteImage(v.getContext(), contact.getImage());
                            ContactListFragment listContactFragment = (ContactListFragment)fragment;
                            listContactFragment.showData();
                        }
                        catch(Exception e) {
                            Toast.makeText(v.getContext(),e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onNoClicked() {
                        //Toast.makeText(getContext(),"Nada", Toast.LENGTH_SHORT).show();
                    }
                };

                DeleteContactDialogFragment dialog = DeleteContactDialogFragment.newInstance("Confirmación", "¿Desea eliminar al contacto '" + contact.getName() + "'?", event);

                final Context context = parent.getContext();
                FragmentManager quote_fm = ((MainActivity)context).getSupportFragmentManager();
                dialog.show(quote_fm, "yesNoAlert");
            }
        });

        nombre.setText(contacts.get(position).getName());
        num.setText(contacts.get(position).getNumber());
        if(contacts.get(position).getImage() != null && contacts.get(position).getImage() != ""){
            Log.d("Imagen", contacts.get(position).getImage());

            File file = new File(contacts.get(position).getImage());
            if(file.exists()) {
                Uri uri = Uri.fromFile(file);
                image.setImageURI(uri);
            }
        }
        //image.setImageResource(R.drawable.perfil);

        return convertView;
    }
}

