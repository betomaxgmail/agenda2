package com.example.agenda2.Models.Contact;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class Contact implements Serializable {
    private int id;
    private String name;
    private String image;
    private String number;
    private String email;
    private String address;
    private Date birthDate;

    public Contact(String name, String number, String img, String email, String address, Date date) {
        this.setName(name);
        this.setNumber(number);
        this.setImage(img);
        this.setEmail(email);
        this.setAddress(address);
        this.setBirthDate(date);
    }

    public Contact(int id, String name, String number, String img, String email, String address, Date date) {
        this.setId(id);
        this.setName(name);
        this.setNumber(number);
        this.setImage(img);
        this.setEmail(email);
        this.setAddress(address);
        this.setBirthDate(date);
    }

    @Override
    public String toString(){
        return this.getName();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }
}
