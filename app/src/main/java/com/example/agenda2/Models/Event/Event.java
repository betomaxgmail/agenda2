package com.example.agenda2.Models.Event;

import java.io.Serializable;
import java.util.Date;

public class Event implements Serializable {
    private int id_evento;
    private String nombre;
    private int contacto;
    private String tipoEvento;
    private Date fecha;
    private String lugar;

    public Event(String nombre, int contacto, String tipoEvento, Date fecha, String lugar) {
        this.setNombre(nombre);
        this.setContacto(contacto);
        this.setTipoEvento(tipoEvento);
        this.setFecha(fecha);
        this.setLugar(lugar);
    }

    public Event(String nombre, Date fecha, String lugar) {
        this.setNombre(nombre);
        this.setFecha(fecha);
        this.setLugar(lugar);
    }


    public Event(int id_evento, String nombre, int contacto, String tipoEvento, Date fecha, String lugar) {
        this.setIdEvento(id_evento);
        this.setNombre(nombre);
        this.setContacto(contacto);
        this.setTipoEvento(tipoEvento);
        this.setFecha(fecha);
        this.setLugar(lugar);
    }
    public int getIdEvento() {
        return id_evento;
    }

    public void setIdEvento(int id_evento) {
        this.id_evento = id_evento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getContacto() {
        return contacto;
    }

    public void setContacto(int contacto) {
        this.contacto = contacto;
    }

    public String getTipoEvento() {
        return tipoEvento;
    }

    public void setTipoEvento(String tipoEvento) {
        this.tipoEvento = tipoEvento;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }
}