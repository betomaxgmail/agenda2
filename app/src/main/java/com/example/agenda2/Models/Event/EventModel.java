package com.example.agenda2.Models.Event;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.example.agenda2.Models.DbHelper;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class EventModel extends DbHelper {

    public EventModel(Context context) {
        super(context);
    }

    public static long insert(Context context, Event event)
    {
        long id = 0;
        SQLiteDatabase db = null;
        try {
            DbHelper dbHelper = new DbHelper(context);
            db = dbHelper.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put("name", event.getNombre());
            values.put("contac", event.getContacto());
            values.put("type_event", event.getTipoEvento());
            if (event.getFecha() != null) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                values.put("date", sdf.format(event.getFecha()));
            }
            values.put("lugar", event.getLugar());


            id = db.insert(TABLE_EVENTOS, null, values);
            event.setIdEvento((int)id);
        } catch (Exception ex) {
            ex.toString();
        } finally {
            if (db != null && db.isOpen())
                db.close();
        }

        return id;
    }

    public static ArrayList<Event> getAll(Context context) {

        DbHelper dbHelper = new DbHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ArrayList<Event> listaEventos = new ArrayList<Event>();
        Event event;
        Cursor cursorEvent;

        cursorEvent = db.rawQuery("SELECT * FROM " + TABLE_EVENTOS, null);

        if (cursorEvent.moveToFirst()) {
            do {
                String dateString = cursorEvent.getString(5);
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                try {
                    Date date = format.parse(dateString);
                    event = new Event(
                            cursorEvent.getInt(0),// id
                            cursorEvent.getString(1),
                            cursorEvent.getInt(2),//contacto
                            cursorEvent.getString(3),
                            date,
                            cursorEvent.getString(4)//fecha
                    );
                    Log.d("GetAll", event.getIdEvento() + " - " + event.getNombre() + " - " + event.getTipoEvento() + " - " + event.getFecha());
                    listaEventos.add(event);
                }
                catch (Exception ex) {
                    Log.d("GetAll", ex.getMessage());
                }
            } while (cursorEvent.moveToNext());
        }

        cursorEvent.close();
        db.close();

        return listaEventos;
    }

    public static Event getById(Context context, int id) {

        DbHelper dbHelper = new DbHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        Event event = null;
        Cursor cursorEvent;

        cursorEvent = db.rawQuery("SELECT * FROM " + TABLE_EVENTOS + " WHERE id_event = " + id + " LIMIT 1", null);

        if (cursorEvent.moveToFirst()) {
            String dateString = cursorEvent.getString(5);
            DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

            try {
                Date date = format.parse(dateString);
                event = new Event(
                        cursorEvent.getInt(0),// id
                        cursorEvent.getString(1),
                        cursorEvent.getInt(2),//contacto
                        cursorEvent.getString(3),
                        date,
                        cursorEvent.getString(4)//fecha
                );
                Log.d("getById", event.getIdEvento() + " - " + event.getNombre() + " - " + event.getTipoEvento() + " - " + event.getFecha());
            }
            catch (Exception ex) {
                Log.d("getById", ex.getMessage());
            }
        }

        cursorEvent.close();
        db.close();

        return event;
    }

    public static boolean update(Context context, Event event) {

        boolean status = false;

        DbHelper dbHelper = new DbHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            db.execSQL("UPDATE " + TABLE_EVENTOS +
                    " SET name = '" + event.getNombre() +
                    "', contac = '" + event.getContacto() +
                    "', type_event = '" + event.getTipoEvento() +
                    "', lugar = '" + event.getLugar() +
                    "', date = '" + sdf.format(event.getFecha()) +
                    "' WHERE id_event='" + event.getIdEvento() + "' ");
            status = true;
        } catch (Exception ex) {
            ex.toString();
            status = false;
        } finally {
            if (db != null && db.isOpen())
                db.close();
        }

        return status;
    }


    public static boolean delete(Context context, Event event) {

        boolean status = false;

        DbHelper dbHelper = new DbHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        try {
            db.execSQL("DELETE FROM " + TABLE_EVENTOS + " WHERE id_event = '" + event.getIdEvento() + "'");
            status = true;
        } catch (Exception ex) {
            ex.toString();
            status = false;
        } finally {
            if (db != null && db.isOpen())
                db.close();
        }

        return status;
    }

}

