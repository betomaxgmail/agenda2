package com.example.agenda2.Models.Event;

import android.content.Context;
import android.net.Uri;
import android.os.Parcel;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.example.agenda2.ContactListFragment;
import com.example.agenda2.EventListFragment;
import com.example.agenda2.MainActivity;
import com.example.agenda2.Modals.DeleteContactDialogFragment;
import com.example.agenda2.Models.Contact.Contact;
import com.example.agenda2.Models.Contact.ContactModel;
import com.example.agenda2.Models.Save;
import com.example.agenda2.R;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class EventAdapter extends ArrayAdapter<Event> {
    private ArrayList<Event> events;
    private androidx.fragment.app.Fragment fragment;

    public EventAdapter(@NonNull Context context, int resource, ArrayList<Event> events, Fragment frag) {
        super(context, resource, events);
        this.events = events;
        this.fragment = frag;
        Log.d("", "Constructor EventAdapter");
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_event_list, parent, false);
        }

        TextView nombreEvento = convertView.findViewById(R.id.nombre_evento);
        TextView fechaEvento = convertView.findViewById(R.id.fechaEvento);
        TextView tipoEvento = convertView.findViewById(R.id.tipoEvento);

        Button btnDelete = convertView.findViewById(R.id.button);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(v.getContext(),String.valueOf(position),Toast.LENGTH_LONG).show();
                Event eventData = events.get(position);

                DeleteContactDialogFragment.OnYesNoClick event = new DeleteContactDialogFragment.OnYesNoClick() {
                    @Override
                    public int describeContents() {
                        return 0;
                    }

                    @Override
                    public void writeToParcel(Parcel dest, int flags) {

                    }

                    @Override
                    public void onYesClicked() {
                        try {
                            EventModel.delete(v.getContext(), eventData);
                            EventListFragment listContactFragment = (EventListFragment)fragment;
                            listContactFragment.showData();
                        }
                        catch(Exception e) {
                            Toast.makeText(v.getContext(),e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onNoClicked() {
                        //Toast.makeText(getContext(),"Nada", Toast.LENGTH_SHORT).show();
                    }
                };

                DeleteContactDialogFragment dialog = DeleteContactDialogFragment.newInstance("Confirmación", "¿Desea eliminar el evento '" + eventData.getNombre() + "'?", event);

                final Context context = parent.getContext();
                FragmentManager quote_fm = ((MainActivity)context).getSupportFragmentManager();
                dialog.show(quote_fm, "yesNoAlert");
            }
        });


        nombreEvento.setText(events.get(position).getNombre());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        fechaEvento.setText(sdf.format(events.get(position).getFecha()));
        tipoEvento.setText(events.get(position).getTipoEvento());

        return convertView;
    }

}